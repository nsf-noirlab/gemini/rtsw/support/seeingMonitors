[schematic2]
uniq 13
[tools]
[detail]
w 1324 2771 100 0 n#1 hb2health.hb2health#52.FLNK 1272 2768 1376 2768 1376 3216 1440 3216 estringouts.estringouts#142.SLNK
w 888 3995 100 0 n#2 hwinl.hwinl#294.in 392 3992 1384 3992 1384 3248 1440 3248 estringouts.estringouts#142.DOL
w 324 4123 100 0 n#3 eais.$(src)getDimmData.FLNK 280 4120 368 4120 368 3816 80 3816 80 2984 368 2984 ecalcs.$(src)Hb.SLNK
w 1728 3235 100 0 n#4 estringouts.estringouts#142.FLNK 1696 3232 1760 3232 1760 3808 1808 3808 monDimmGea.monDimmGea#290.SLNK
w 692 3179 100 0 n#5 ecalcs.$(src)Hb.VAL 648 3176 736 3176 736 3448 192 3448 192 3368 360 3368 ecalcs.$(src)Hb.INPA
w 34 4139 100 0 n#6 hwinxl.hwinxl#293.in 31 4136 31 4136 eais.$(src)getDimmData.INP
w 756 3211 100 0 n#7 ecalcs.$(src)Hb.FLNK 648 3208 864 3208 864 2768 936 2768 hb2health.hb2health#52.SLNK
w 872 2867 100 0 n#5 hb2health.hb2health#52.HB 936 2864 808 2864 808 3176 736 3176 junction
w 184 3339 100 0 n#8 hwinl.hwinl#296.in 8 3336 360 3336 ecalcs.$(src)Hb.INPB
w 1308 1771 100 0 n#9 egenSubC.$(src)Gs.FLNK 1272 1768 1344 1768 1344 1592 16 1592 16 2064 200 2064 elongouts.$(src)Hb2.SLNK
w 691 2572 100 0 n#10 hwinl.hwinl#299.in 688 2864 688 2280 16 2280 16 2096 200 2096 elongouts.$(src)Hb2.DOL
w 588 2035 100 0 n#11 elongouts.$(src)Hb2.OUT 456 2032 720 2032 720 1648 904 1648 hwoutl.hwoutl#300.outp
w -136 3979 100 0 n#12 timer.timer#301.FLNK -168 3976 -104 3976 -104 4104 24 4104 eais.$(src)getDimmData.SLNK
s 3008 1280 150 0 16/Jul/04
s 2992 1360 150 0 Pedro Gigoux
s 3568 1472 200 0 Seeing Monitor Interface
s 3600 1616 200 0 Gemini Weather Server
s 2975 4286 200 0 Updated for new DIMM mySQL database
s 3847 4287 200 0 2018-05-10
s 4127 4287 200 0 Mike Westfall
s 2424 3610 100 0 This record is obsolete, but kept for backward compatibility for GEA
[cell use]
use bd200tr -768 1128 -100 0 frame
xform 0 1872 2832
use monDimmGea 1840 3719 100 0 monDimmGea#290
xform 0 1984 3808
p 1936 3712 100 0 1 setg:gea see
use eaos 3128 2247 100 0 eaos#71
xform 0 3256 2336
p 3200 2305 100 0 1 EGU:
p 2872 2318 100 0 0 OMSL:supervisory
p 3200 2280 100 0 1 PREC:3
p 3240 2240 100 1024 -1 name:$(top)$(src)StrhlR
use eaos 2648 2479 100 0 eaos#178
xform 0 2776 2568
p 2721 2540 100 0 1 EGU:
p 2392 2550 100 0 0 OMSL:supervisory
p 2723 2512 100 0 1 PREC:3
p 2760 2472 100 1024 -1 name:$(top)$(src)ScintL
use eaos 3112 3343 100 0 eaos#177
xform 0 3240 3432
p 3182 3405 100 0 1 EGU:
p 2856 3414 100 0 0 OMSL:supervisory
p 3183 3378 100 0 1 PREC:3
p 3224 3336 100 1024 -1 name:$(top)$(src)Secz
use eaos 2648 3143 100 0 eaos#175
xform 0 2776 3232
p 2717 3206 100 0 1 EGU:arcsec
p 2392 3214 100 0 0 OMSL:supervisory
p 2719 3175 100 0 1 PREC:3
p 2760 3136 100 1024 -1 name:$(top)$(src)FwhmX
use eaos 2064 2919 100 0 eaos#173
xform 0 2192 3008
p 2135 2981 100 0 1 EGU:cm
p 1808 2990 100 0 0 OMSL:supervisory
p 2135 2952 100 0 1 PREC:3
p 2176 2912 100 1024 -1 name:$(top)$(src)R0
use eaos 2064 3135 100 0 eaos#172
xform 0 2192 3224
p 2135 3198 100 0 1 EGU:arcsec
p 1808 3206 100 0 0 OMSL:supervisory
p 2135 3169 100 0 1 PREC:3
p 2176 3128 100 1024 -1 name:$(top)$(src)Fwhm
use eaos 2064 2679 100 0 eaos#170
xform 0 2192 2768
p 2135 2740 100 0 1 EGU:pixels
p 1808 2750 100 0 0 OMSL:supervisory
p 2136 2713 100 0 1 PREC:1
p 2176 2672 100 1024 -1 name:$(top)$(src)Dy
use eaos 3120 2919 100 0 eaos#169
xform 0 3248 3008
p 3189 2984 100 0 1 EGU:pixels
p 2864 2990 100 0 0 OMSL:supervisory
p 3190 2954 100 0 1 PREC:1
p 3232 2912 100 1024 -1 name:$(top)$(src)Dx
use eaos 3120 3143 100 0 eaos#70
xform 0 3248 3232
p 3189 3205 100 0 1 EGU:arcsec
p 2864 3214 100 0 0 OMSL:supervisory
p 3189 3178 100 0 1 PREC:3
p 3232 3136 100 1024 -1 name:$(top)$(src)FwhmY
use eaos 3128 2479 100 0 eaos#73
xform 0 3256 2568
p 3202 2540 100 0 1 EGU:
p 2872 2550 100 0 0 OMSL:supervisory
p 3202 2514 100 0 1 PREC:3
p 3240 2472 100 1024 -1 name:$(top)$(src)StrhlL
use eaos 2648 2239 100 0 eaos#72
xform 0 2776 2328
p 2720 2297 100 0 1 EGU:
p 2392 2310 100 0 0 OMSL:supervisory
p 2720 2271 100 0 1 PREC:3
p 2760 2232 100 1024 -1 name:$(top)$(src)ScintR
use estringouts 2072 3367 100 0 estringouts#165
xform 0 2200 3440
p 2008 3246 100 0 0 OMSL:supervisory
p 2184 3360 100 1024 -1 name:$(top)$(src)TimeUTC
use estringouts 1440 3143 100 0 estringouts#142
xform 0 1568 3216
p 1376 3022 100 0 0 OMSL:closed_loop
p 1552 3136 100 1024 -1 name:$(top)$(src)ErrMsg
use hb2health 960 2711 100 0 hb2health#52
xform 0 1104 2832
p 1069 2742 100 0 1 set0:dev $(src)
use eais 142 4027 100 0 $(src)getDimmData
xform 0 152 4104
p 58 4027 100 0 -1 PV:$(top)
p 94 4166 100 0 1 DTYP:stream
p 95 4192 100 0 1 SCAN:Passive
p 94 4219 100 0 1 PINI:YES
use hwinxl -544 4064 100 0 hwinxl#293
xform 0 -256 4112
p -534 4128 100 0 -1 val(in):@monDimm.proto getDimmData($(top)$(src)) SEEMON
use hwinl 16 3920 100 0 hwinl#294
xform 0 208 3968
p 26 3984 100 0 -1 val(in):$(top)$(src)getDimmData.STAT
use ecalcs 455 2904 100 0 $(src)Hb
xform 0 504 3160
p 371 2904 100 0 -1 PV:$(top)
p 499 3354 100 0 1 CALC:(B==3)?A:(A+1)
use hwinl -368 3264 100 0 hwinl#296
xform 0 -176 3312
p -358 3328 100 0 -1 val(in):$(top)$(src)getDimmData.SEVR
use elongouts 317 1964 100 0 $(src)Hb2
xform 0 328 2064
p 233 1964 100 0 -1 PV:$(top)
p 259 2147 100 0 1 OMSL:closed_loop
use hwinl 312 2792 100 0 hwinl#299
xform 0 504 2840
p 322 2856 100 0 -1 val(in):$(top)$(src)Hb.VAL
use hwoutl 904 1576 100 0 hwoutl#300
xform 0 1096 1624
p 1002 1639 100 0 -1 val(outp):$(top)$(src)Gs.VALU
use egenSubC 1142 1707 100 0 $(src)Gs
xform 0 1128 2136
p 1058 1707 100 0 -1 PV:$(top)
p 1083 1857 100 0 1 FTVU:LONG
p 1048 2553 100 0 1 SCAN:1 second
use timer -360 3832 100 0 timer#301
xform 0 -256 3936
p -368 3878 100 0 1 setdev:dev $(src)gdd
use elongouts 2648 3368 100 0 elongouts#302
xform 0 2776 3432
p 2760 3336 100 1024 -1 name:$(top)$(src)Hr
use estringouts 2080 3543 100 0 estringouts#304
xform 0 2208 3616
p 2016 3422 100 0 0 OMSL:supervisory
p 2192 3536 100 1024 -1 name:$(top)$(src)DateUT
use elongouts 2648 2952 100 0 elongouts#306
xform 0 2776 3016
p 2760 2920 100 1024 -1 name:$(top)$(src)Nimg
use elongouts 2064 2264 100 0 elongouts#308
xform 0 2192 2328
p 2176 2232 100 1024 -1 name:$(top)$(src)FluxR
p 2143 2277 100 0 1 EGU:
use elongouts 2064 2504 100 0 elongouts#310
xform 0 2192 2568
p 2176 2472 100 1024 -1 name:$(top)$(src)FluxL
p 2146 2512 100 0 1 EGU:
use elongouts 2640 2704 100 0 elongouts#312
xform 0 2768 2768
p 2752 2672 100 1024 -1 name:$(top)$(src)Flux
p 2730 2722 100 0 1 EGU:
[comments]
