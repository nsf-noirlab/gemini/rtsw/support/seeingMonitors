[schematic2]
uniq 2
[tools]
[detail]
w 3698 2682 -100 0 n#1 inhier.SLNK.P 3696 2672 3808 2672 outhier.FLNK.p
s 3706 1881 150 0 Gemini Weather Server
s 3770 1824 100 0 Dimm Interface to GEA
s 3489 1736 100 0 16/Jul/04
s 3461 1772 100 0 Pedro Gigoux
s 2176 3616 100 0 The purpose of this schematic is to provide backwards compatibilty with GEA
s 2176 3584 100 0 since the DIMM records changed their names when the interfaces to MASS and IRMA
s 2176 3552 100 0 were added to the system
[cell use]
use bc200tr 864 1608 -100 0 frame
xform 0 2544 2912
use inhier 3680 2631 100 0 SLNK
xform 0 3696 2672
use eaos 1408 2871 100 0 eaos#72
xform 0 1536 2960
p 1480 2929 100 0 1 EGU:
p 1152 2942 100 0 0 OMSL:closed_loop
p 1480 2903 100 0 1 PREC:3
p 1520 2864 100 1024 -1 name:$(top)$(gea)Strlh1t1
use eaos 1408 2423 100 0 eaos#74
xform 0 1536 2512
p 1482 2484 100 0 1 EGU:
p 1152 2494 100 0 0 OMSL:closed_loop
p 1482 2458 100 0 1 PREC:3
p 1520 2416 100 1024 -1 name:$(top)$(gea)Strlh1t2
use eaos 1408 2647 100 0 eaos#73
xform 0 1536 2736
p 1482 2708 100 0 1 EGU:
p 1152 2718 100 0 0 OMSL:closed_loop
p 1482 2682 100 0 1 PREC:3
p 1520 2640 100 1024 -1 name:$(top)$(gea)Strlh2t1
use eaos 2848 2279 100 0 eaos#70
xform 0 2976 2368
p 2917 2341 100 0 1 EGU:arcsec
p 2592 2350 100 0 0 OMSL:closed_loop
p 2917 2314 100 0 1 PREC:3
p 2960 2272 100 1024 -1 name:$(top)$(gea)FwhmY
use eaos 2144 2695 100 0 eaos#169
xform 0 2272 2784
p 2213 2760 100 0 1 EGU:pixels
p 1888 2766 100 0 0 OMSL:closed_loop
p 2214 2730 100 0 1 PREC:1
p 2256 2688 100 1024 -1 name:$(top)$(gea)MeanDx
use eaos 2144 2471 100 0 eaos#170
xform 0 2272 2560
p 2215 2532 100 0 1 EGU:pixels
p 1888 2542 100 0 0 OMSL:closed_loop
p 2216 2505 100 0 1 PREC:1
p 2256 2464 100 1024 -1 name:$(top)$(gea)MeanDy
use eaos 2848 2503 100 0 eaos#172
xform 0 2976 2592
p 2919 2566 100 0 1 EGU:arcsec
p 2592 2574 100 0 0 OMSL:closed_loop
p 2919 2537 100 0 1 PREC:3
p 2960 2496 100 1024 -1 name:$(top)$(gea)Fwhm
use eaos 2144 3143 100 0 eaos#173
xform 0 2272 3232
p 2215 3205 100 0 1 EGU:cm
p 1888 3214 100 0 0 OMSL:closed_loop
p 2215 3176 100 0 1 PREC:3
p 2256 3136 100 1024 -1 name:$(top)$(gea)R0
use eaos 2848 2727 100 0 eaos#175
xform 0 2976 2816
p 2917 2790 100 0 1 EGU:arcsec
p 2592 2798 100 0 0 OMSL:closed_loop
p 2919 2759 100 0 1 PREC:3
p 2960 2720 100 1024 -1 name:$(top)$(gea)FwhmX
use eaos 1408 2199 100 0 eaos#176
xform 0 1536 2288
p 1480 2259 100 0 1 EGU:
p 1152 2270 100 0 0 OMSL:closed_loop
p 1480 2231 100 0 1 PREC:3
p 1520 2192 100 1024 -1 name:$(top)$(gea)Strlh2t2
use eaos 2848 3175 100 0 eaos#177
xform 0 2976 3264
p 2918 3237 100 0 1 EGU:
p 2592 3246 100 0 0 OMSL:closed_loop
p 2919 3210 100 0 1 PREC:3
p 2960 3168 100 1024 -1 name:$(top)$(gea)Secz
use eaos 2144 2247 100 0 eaos#178
xform 0 2272 2336
p 2217 2308 100 0 1 EGU:
p 1888 2318 100 0 0 OMSL:closed_loop
p 2219 2280 100 0 1 PREC:3
p 2256 2240 100 1024 -1 name:$(top)$(gea)Sc1t1
use eaos 1408 3095 100 0 eaos#71
xform 0 1536 3184
p 1480 3153 100 0 1 EGU:
p 1152 3166 100 0 0 OMSL:closed_loop
p 1480 3128 100 0 1 PREC:3
p 1520 3088 100 1024 -1 name:$(top)$(gea)Sc2t1
use estringouts 3648 3223 100 0 estringouts#164
xform 0 3776 3296
p 3584 3102 100 0 0 OMSL:supervisory
p 3760 3216 100 1024 -1 name:$(top)$(gea)StarName
p 3584 3262 100 0 0 VAL:NONE
use estringouts 3648 2999 100 0 estringouts#165
xform 0 3776 3072
p 3584 2878 100 0 0 OMSL:closed_loop
p 3760 2992 100 1024 -1 name:$(top)$(gea)DateUT
p 3766 3309 100 0 0 VAL:2019-00-00
use outhier 3774 2630 100 0 FLNK
xform 0 3792 2672
use elongouts 2144 2944 100 0 elongouts#327
xform 0 2272 3008
p 2256 2912 100 1024 -1 name:$(top)$(gea)MeanFlux
p 1984 2926 100 0 0 OMSL:closed_loop
p 2232 2959 100 0 1 EGU:
use elongouts 2848 2976 100 0 elongouts#328
xform 0 2976 3040
p 2960 2944 100 1024 -1 name:$(top)$(gea)Nimg
p 2934 2992 100 0 1 EGU:
[comments]
