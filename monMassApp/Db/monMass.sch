[schematic2]
uniq 12
[tools]
[detail]
w 1460 2763 100 0 n#1 hb2health.hb2health#52.FLNK 1400 2760 1520 2760 estringouts.estringouts#142.SLNK
w 1100 3739 100 0 n#2 hwinl.hwinl#306.in 704 3736 1496 3736 1496 2792 1520 2792 estringouts.estringouts#142.DOL
w 756 2731 100 0 n#3 ecalcs.$(src)Hb.VAL 712 2728 800 2728 800 3000 256 3000 256 2920 424 2920 ecalcs.$(src)Hb.INPA
w 884 2763 100 0 n#4 ecalcs.$(src)Hb.FLNK 712 2760 1056 2760 hb2health.hb2health#52.SLNK
w 960 2859 100 0 n#3 hb2health.hb2health#52.HB 1048 2856 872 2856 872 2728 800 2728 junction
w 248 2891 100 0 n#5 hwinl.hwinl#310.in 72 2888 424 2888 ecalcs.$(src)Hb.INPB
w 338 3883 100 0 n#6 hwinxl.hwinxl#304.in 335 3880 335 3880 eais.$(src)getMassData.INP
w 788 3867 100 0 n#7 eais.$(src)getMassData.FLNK 592 3864 984 3864 984 3280 152 3280 152 2536 424 2536 ecalcs.$(src)Hb.SLNK
w 1356 1779 100 0 n#8 egenSubC.$(src)Gs.FLNK 1320 1776 1392 1776 1392 1600 632 1600 632 1888 88 1888 88 2096 424 2096 elongouts.$(src)Hb2.SLNK
w 256 2131 100 0 n#9 elongouts.$(src)Hb2.DOL 424 2128 88 2128 88 2368 888 2368 888 2416 856 2416 hwinl.hwinl#316.in
w 712 2067 100 0 n#10 elongouts.$(src)Hb2.OUT 680 2064 744 2064 744 1672 936 1672 hwoutl.hwoutl#318.outp
w 50 3730 -100 0 n#11 timer.timer#319.FLNK 40 3728 168 3728 168 3848 336 3848 eais.$(src)getMassData.SLNK
s 3008 1280 150 0 16/Jul/04
s 2992 1360 150 0 Pedro Gigoux
s 3568 1472 200 0 Seeing Monitor Interface
s 3600 1616 200 0 Gemini Weather Server
[cell use]
use bd200tr -768 1128 -100 0 frame
xform 0 1872 2832
use eaos 2072 2231 100 0 eaos#299
xform 0 2200 2320
p 2040 2046 100 0 0 EGU:
p 1816 2302 100 0 0 OMSL:closed_loop
p 2184 2224 100 1024 -1 name:$(top)$(src)Valid
use eaos 2080 3111 100 0 eaos#71
xform 0 2208 3200
p 2152 3169 100 0 1 EGU:arcsec
p 1824 3182 100 0 0 OMSL:supervisory
p 2152 3144 100 0 1 PREC:3
p 2192 3104 100 1024 -1 name:$(top)$(src)Fsee
use eaos 3264 3175 100 0 eaos#177
xform 0 3392 3264
p 3334 3237 100 0 1 EGU:ms
p 3008 3246 100 0 0 OMSL:supervisory
p 3335 3210 100 0 1 PREC:2
p 3376 3168 100 1024 -1 name:$(top)$(src)Tau
use eaos 3264 2727 100 0 eaos#175
xform 0 3392 2816
p 3333 2790 100 0 1 EGU:Km
p 3008 2798 100 0 0 OMSL:supervisory
p 3335 2759 100 0 1 PREC:2
p 3376 2720 100 1024 -1 name:$(top)$(src)J2X
use eaos 2656 2919 100 0 eaos#174
xform 0 2784 3008
p 2725 2978 100 0 1 EGU:
p 2400 2990 100 0 0 OMSL:supervisory
p 2768 2912 100 1024 -1 name:$(top)$(src)LayerX
use eaos 2656 3143 100 0 eaos#173
xform 0 2784 3232
p 2727 3205 100 0 1 EGU:arcsec
p 2400 3214 100 0 0 OMSL:supervisory
p 2727 3176 100 0 1 PREC:2
p 2768 3136 100 1024 -1 name:$(top)$(src)Isop1
use eaos 3264 2503 100 0 eaos#172
xform 0 3392 2592
p 3335 2566 100 0 1 EGU:Km
p 3008 2574 100 0 0 OMSL:closed_loop
p 3335 2537 100 0 1 PREC:2
p 3376 2496 100 1024 -1 name:$(top)$(src)J16X
use eaos 3264 2951 100 0 eaos#171
xform 0 3392 3040
p 3334 3011 100 0 1 EGU:
p 3008 3022 100 0 0 OMSL:supervisory
p 3376 2944 100 1024 -1 name:$(top)$(src)Chi2X
use eaos 2656 2471 100 0 eaos#170
xform 0 2784 2560
p 2727 2532 100 0 1 EGU:Km
p 2400 2542 100 0 0 OMSL:supervisory
p 2728 2505 100 0 1 PREC:1
p 2768 2464 100 1024 -1 name:$(top)$(src)J8X
use eaos 2656 2695 100 0 eaos#169
xform 0 2784 2784
p 2725 2760 100 0 1 EGU:Km
p 2400 2766 100 0 0 OMSL:supervisory
p 2726 2730 100 0 1 PREC:1
p 2768 2688 100 1024 -1 name:$(top)$(src)J1X
use eaos 2080 2663 100 0 eaos#73
xform 0 2208 2752
p 2154 2724 100 0 1 EGU:Km
p 1824 2734 100 0 0 OMSL:supervisory
p 2154 2698 100 0 1 PREC:3
p 2192 2656 100 1024 -1 name:$(top)$(src)J05X
use eaos 2080 2439 100 0 eaos#74
xform 0 2208 2528
p 2154 2500 100 0 1 EGU:Km
p 1824 2510 100 0 0 OMSL:supervisory
p 2154 2474 100 0 1 PREC:3
p 2192 2432 100 1024 -1 name:$(top)$(src)J4X
use eaos 2080 2887 100 0 eaos#72
xform 0 2208 2976
p 2152 2945 100 0 1 EGU:
p 1824 2958 100 0 0 OMSL:supervisory
p 2152 2919 100 0 1 PREC:3
p 2192 2880 100 1024 -1 name:$(top)$(src)Airmass
use estringouts 3776 2999 100 0 estringouts#165
xform 0 3904 3072
p 3712 2878 100 0 0 OMSL:supervisory
p 3888 2992 100 1024 -1 name:$(top)$(src)Time
use estringouts 3776 3223 100 0 estringouts#164
xform 0 3904 3296
p 3712 3102 100 0 0 OMSL:supervisory
p 3888 3216 100 1024 -1 name:$(top)$(src)Date
use estringouts 1520 2687 100 0 estringouts#142
xform 0 1648 2760
p 1456 2566 100 0 0 OMSL:closed_loop
p 1632 2680 100 1024 -1 name:$(top)$(src)ErrMsg
use hb2health 1186 2704 100 0 hb2health#52
xform 0 1224 2824
p 1189 2734 100 0 1 set0:dev $(src)
use eais 470 3761 100 0 $(src)getMassData
xform 0 464 3848
p 379 3761 100 0 -1 PV:$(top)
p 406 3910 100 0 1 DTYP:stream
p 407 3936 100 0 1 SCAN:Passive
p 406 3963 100 0 1 PINI:YES
use hwinxl -240 3808 100 0 hwinxl#304
xform 0 48 3856
p -230 3872 100 0 -1 val(in):@monMass.proto getMassData($(top)$(src)) SEEMON
use hwinl 328 3664 100 0 hwinl#306
xform 0 520 3712
p 338 3728 100 0 -1 val(in):$(top)$(src)getMassData.STAT
use ecalcs 519 2456 100 0 $(src)Hb
xform 0 568 2712
p 435 2456 100 0 -1 PV:$(top)
p 563 2906 100 0 1 CALC:(B==3)?A:(A+1)
use hwinl -304 2816 100 0 hwinl#310
xform 0 -112 2864
p -294 2880 100 0 -1 val(in):$(top)$(src)getMassData.SEVR
use elongouts 541 1996 100 0 $(src)Hb2
xform 0 552 2096
p 457 1996 100 0 -1 PV:$(top)
p 483 2179 100 0 1 OMSL:closed_loop
use hwinl 480 2344 100 0 hwinl#316
xform 0 672 2392
p 490 2408 100 0 -1 val(in):$(top)$(src)Hb.VAL
use hwoutl 936 1600 100 0 hwoutl#318
xform 0 1128 1648
p 1034 1663 100 0 -1 val(outp):$(top)$(src)Gs.VALU
use egenSubC 1204 1721 100 0 $(src)Gs
xform 0 1176 2144
p 1120 1721 100 0 -1 PV:$(top)
p 1098 2558 100 0 1 SCAN:1 second
p 1143 1864 100 0 1 FTVU:LONG
use timer -144 3584 100 0 timer#319
xform 0 -40 3688
p -152 3630 100 0 1 setdev:dev $(src)gmd
[comments]
